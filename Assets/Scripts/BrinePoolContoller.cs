﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrinePoolContoller : MonoBehaviour
{
    public GameObject brineText;
    public GameObject Player;
    public float brineTextDistance;

    private void Start()
    {
        brineText.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<PlayerController>().currentState = PlayerController.PlayerState.Brine;
            brineText.SetActive(true);
        }
    }

    private void Update()
    {
        brineText.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y + brineTextDistance, Player.transform.position.z);
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<PlayerController>().currentState = PlayerController.PlayerState.Normal;
            brineText.SetActive(false);
        }
    }
}
