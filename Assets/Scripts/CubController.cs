﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubController : MonoBehaviour
{
    Sprite defaultSprite;
    public Sprite cheerSprite;


    // Start is called before the first frame update
    void Start()
    {
        defaultSprite = GetComponent<SpriteRenderer>().sprite;
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            GetComponent<SpriteRenderer>().sprite = cheerSprite;
            GameController.gameControllerInstance.PlayerWin();
        }
    }  
}
