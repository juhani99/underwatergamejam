﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public Vector3 movementVector;
    public float movementSpeed;
    SpriteRenderer sprite;
    public Sprite[] sprites;
    
    public float spriteSwapTimer;
    float spriteSwapTimerReset;

    public float swapDirectionTimer;
    float swapDirectionTimerReset;

    // Start is called before the first frame update
    void Start()
    {
        spriteSwapTimerReset = spriteSwapTimer;
        swapDirectionTimerReset = swapDirectionTimer;
        sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (spriteSwapTimer > 0)
        {
            spriteSwapTimer -= Time.deltaTime;
            if (spriteSwapTimer <= 0)
            {
                SwapSprite();
                spriteSwapTimer = spriteSwapTimerReset;
            }
        }

        if (swapDirectionTimer > 0)
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, 180, transform.eulerAngles.z);
            swapDirectionTimer -= Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, transform.position + movementVector, movementSpeed * Time.deltaTime);
        }
        else if (swapDirectionTimer > -swapDirectionTimerReset)
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, 0, transform.eulerAngles.z);
            swapDirectionTimer -= Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, transform.position - movementVector, movementSpeed * Time.deltaTime);
        }
        else if (swapDirectionTimer <= -swapDirectionTimerReset)
        {
            swapDirectionTimer = swapDirectionTimerReset;
        }
        
    }

    void SwapSprite()
    {
        if (sprite.sprite == sprites[0])
        {
            sprite.sprite = sprites[1];
        }
        else
        {
            sprite.sprite = sprites[0];
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<PlayerController>().TakeDamage(1, gameObject);
        }
    }
}
