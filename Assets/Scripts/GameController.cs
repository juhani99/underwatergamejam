﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    Scene currentScene;
    GameObject player;
    Animator animator;
    public float gravityScale;
    public Image[] playerHealth;
    public Image brineImage;
    public float brineAlphaSpeed;

    PlayerController playerControllerScript;
    public GameObject PlayerCanvas;

    public static GameController gameControllerInstance;

    public Image winImage;

    public float winAlphaSpeed;

    public GameObject startGameUI;

    public GameObject brineText;

    private void Awake()
    {
        if (gameControllerInstance == null)
        {
            gameControllerInstance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        currentScene = SceneManager.GetActiveScene();
        animator = GetComponent<Animator>();
        playerControllerScript = player.GetComponent<PlayerController>();
        animator.enabled = false;
    }

    public void StartGame()
    {
        startGameUI.SetActive(false);
        animator.enabled = true;
    }

    public void PlayerBrineState()
    {
        brineImage.color = new Color(1,1,1, brineImage.color.a + Time.deltaTime * brineAlphaSpeed);

        print("Color " + brineImage.color.a);

        if (brineImage.color.a > 0.7f)
        {
            brineText.SetActive(false);
            playerControllerScript.currentState = PlayerController.PlayerState.Dead;
        }
    }

    public void PlayerNormalState()
    {
        if (brineImage.color.a > 0)
        {
            brineImage.color = new Color(1, 1, 1, brineImage.color.a - Time.deltaTime * brineAlphaSpeed);
        }

      //  print("Color " + brineImage.color.a);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(currentScene.name);
    }

    public void EndCameraPan()
    {
        player.GetComponent<Rigidbody2D>().gravityScale = gravityScale;
        playerControllerScript.enabled = true;
        PlayerCanvas.SetActive(true);
        animator.enabled = false;
    }

    public void UpdateHealth()
    {
        int healthDifference = playerControllerScript.maxHealth - playerControllerScript.health;

        for (int i = 0; i < healthDifference; i++)
        {
            playerHealth[playerHealth.Length-healthDifference].enabled = false;
        }
    }

    public void PlayerWin()
    {
        playerControllerScript.enabled = false;
        player.GetComponent<Rigidbody2D>().gravityScale = 0;


        if (winImage.color.a >= 1)
        {
            RestartGame();
        }
        else
        {
            winImage.color = new Color(winImage.color.r, winImage.color.g, winImage.color.b, winImage.color.a + Time.deltaTime * winAlphaSpeed);
        }
    }
}
