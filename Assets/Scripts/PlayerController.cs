﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public enum PlayerState { Normal, Damaged, Dead, Brine, Empty };
    public PlayerState currentState;

    public Vector3 velocity;
    public SpriteRenderer playerSprite;
    public float rotationSpeed;
    public int health;
    public int maxHealth;

    public Sprite[] normalSprites;
    public Sprite[] damagedSprites;
    public Sprite deadSprite;
    public float deadRotSpeed;
    public float deadTimer;
    GameObject mainCamera;

    Rigidbody2D rb;

    public float spriteSwapTimer;
    float spriteSwapTimerReset;

    public int swapCounter;
    public int swapCounterCap;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        rb = GetComponent<Rigidbody2D>();
        spriteSwapTimerReset = spriteSwapTimer;
        maxHealth = health;
        currentState = PlayerState.Normal;
    }

    // Update is called once per fram
    void Update()
    {
        mainCamera.transform.position = new Vector3(transform.position.x, transform.position.y, -9);

       // rb.freezeRotation = true;

        switch (currentState)
        {
            case PlayerState.Normal:

                Movement();
                GameController.gameControllerInstance.PlayerNormalState();

                if (spriteSwapTimer > 0)
                {
                    spriteSwapTimer -= Time.deltaTime;
                    if (spriteSwapTimer <= 0)
                    {
                        SwapSprite(normalSprites);
                        spriteSwapTimer = spriteSwapTimerReset;
                    }
                }

                if (Input.GetKeyDown(KeyCode.Space) && !SoundManager.SMInstace.AS.isPlaying) //Launch player
                {
                    SoundManager.SMInstace.PlayerSwimSound();
                    rb.velocity = transform.TransformDirection(velocity);
                    SwapSprite(normalSprites);
                }

                break;
            case PlayerState.Damaged:

                if (spriteSwapTimer > 0)
                {
                    spriteSwapTimer -= Time.deltaTime;
                    if (spriteSwapTimer <= 0)
                    {
                        swapCounter++;
                        SwapSprite(damagedSprites);
                        spriteSwapTimer = spriteSwapTimerReset;

                        if (swapCounter >= swapCounterCap)
                        {
                            swapCounter = 0;
                            currentState = PlayerState.Normal;
                        }

                    }
                }

                break;
            case PlayerState.Dead:

                playerSprite.sprite = deadSprite;

                Vector3 deadRotation;
                deadRotation = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 180);

                transform.Rotate(Vector3.forward * deadRotSpeed * Time.deltaTime);

                if (Mathf.RoundToInt(transform.eulerAngles.z) == deadRotation.z || Mathf.RoundToInt(transform.eulerAngles.z) == -deadRotation.z)
                {
                    currentState = PlayerState.Empty;
                }

                break;

            case PlayerState.Brine:

                Movement();
                GameController.gameControllerInstance.PlayerBrineState();

                if (spriteSwapTimer > 0)
                {
                    spriteSwapTimer -= Time.deltaTime;
                    if (spriteSwapTimer <= 0)
                    {
                        SwapSprite(normalSprites);
                        spriteSwapTimer = spriteSwapTimerReset;
                    }
                }

                if (Input.GetKeyDown(KeyCode.Space) && !SoundManager.SMInstace.AS.isPlaying) //Launch player
                {
                    SoundManager.SMInstace.PlayerSwimSound();
                    rb.velocity = transform.TransformDirection(velocity / 2);
                    SwapSprite(normalSprites);
                }

                break;

            case PlayerState.Empty:

                deadTimer -= Time.deltaTime;
                if (deadTimer < 0)
                {
                    GameController.gameControllerInstance.RestartGame();
                }
                break;
        }
    }

    void Movement()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.eulerAngles = new Vector3(0, 180, transform.eulerAngles.z);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.z);
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z + rotationSpeed * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z - rotationSpeed * Time.deltaTime);
        }
    }

    void SwapSprite(Sprite[] swapSprite)
    {
        if (playerSprite.sprite == swapSprite[0])
        {
            playerSprite.sprite = swapSprite[1];
        }
        else
        {
            playerSprite.sprite = swapSprite[0];
        }
    }

    public void TakeDamage(int damageAmount, GameObject Object)
    {
        switch (currentState)
        {
            case PlayerState.Normal:
                
                health -= damageAmount;
                GameController.gameControllerInstance.UpdateHealth();
                currentState = PlayerState.Damaged;

                SoundManager.SMInstace.PlayerTakeDamageSound();

                if (transform.position.y > Object.transform.position.y)
                {
                    rb.velocity = new Vector2(0, 7);
                }
                else
                {
                    rb.velocity = new Vector2(0, -7);
                }

                if (health < 1)
                {
                    print("player dead");
                    currentState = PlayerState.Dead;
                    rb.gravityScale = -rb.gravityScale;
                }
                break;
            case PlayerState.Damaged:

                print("not taking damage when in damaged state");
                break;
            case PlayerState.Dead:

                print("not taking damage when in dead state");
                break;
            case PlayerState.Empty:

                print("not taking damage when in empty state");
                break;
        }
    }
}
