﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioClip takeDamageClip, swimClip;
    public AudioSource AS;

    public static SoundManager SMInstace;

    private void Awake()
    {
        if (SMInstace == null)
        {
            SMInstace = this;
            DontDestroyOnLoad(gameObject);
        }
        else 
        {
            Destroy(gameObject);
        }
        
    }

    // Start is called before the first frame update
    void Start()
    {
        AS = GetComponent<AudioSource>();
    }

    public void PlayerTakeDamageSound()
    {
        AS.clip = takeDamageClip;
        AS.Play();
    }

    public void PlayerSwimSound()
    {
        if (!AS.isPlaying)
        {
            AS.clip = swimClip;
            AS.Play();
        }
    }
}
